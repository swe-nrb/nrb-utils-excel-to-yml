var XLSX = require('xlsx');
var _ = require('lodash');
const uuidv4 = require('uuid/v4');
var slug = require('slug');
var fs = require('fs');

var wb = XLSX.readFile('SBE-Begrepp-bruttolista-20210115.xlsx');
var sheet = wb.Sheets[wb.SheetNames[0]];

var items = XLSX.utils.sheet_to_json(sheet);

console.log(`Found ${items.length} of rows to process`);

var skipped = [];
var files = [];
var badConcepts = [];
_.forEach(items, (item) => {
    if(item["NR-status"] === "X") {
        skipped.push(item);
        return;
    }

    let swedishConcept = null;
    let englishConcept = null;
    
    if(item.sv_term) {

        if(!item.sv_source) {
            badConcepts.push({
                reason: "Missing source",
                concept: item
            });
            return;
        }

        var tmpStr = item.sv_term + '-' + item.sv_source;
        tmpStr = tmpStr.substr(0,60);
        var newSlug = slug(tmpStr);
        newSlug = newSlug.toLowerCase();
        
        swedishConcept = {
            id: uuidv4(),
            _schema: "http://schemas.nationella-riktlinjer.se/nrb-concept.2.schema.json",
            slug: newSlug,
            language: "sv",
            term: item.sv_term.replace("\r\n", " ").trim(),
            source: item.sv_source.replace("\r\n", " ").trim(),
            definition: item.sv_definition.trim(),
            __filename: newSlug + '.json'
        };

        if(item.sv_source_long) {
            swedishConcept.source_long = item.sv_source_long.replace("\r\n", " ").trim();
        }

        if(item.sv_comment) {
            swedishConcept.comment = item.sv_comment.replace("\r\n", " ").trim();
        }

        let qualityTag = {
            id: uuidv4(),
            _schema: "http://schemas.nationella-riktlinjer.se/nrb-tag.1.schema.json",
            tag_definition: "nrb-tag-definition-quality",
            concept: swedishConcept.slug,
            language: "sv",
            review_steps: [
              {
                name: "Granskad",
                date: "2021-02-15",
                author: {
                  name: "Johan Asplund",
                  linkedin: "https://www.linkedin.com/in/bimjohan/"
                }
              }
            ],
            __filename: "nrb-tag-definition-quality-" + swedishConcept.slug + '.json'
          };
          files.push(qualityTag);

          let nrbTag = {
            id: uuidv4(),
            _schema: "http://schemas.nationella-riktlinjer.se/nrb-tag.1.schema.json",
            tag_definition: "nrb-tag-definition-nrb",
            concept: swedishConcept.slug,
            language: "sv",
            review_steps: [
                {
                    name: "Kandidat",
                    date: "2021-02-15",
                    author: {
                      name: "Johan Asplund",
                      linkedin: "https://www.linkedin.com/in/bimjohan/"
                    }
                  }, {
                    name: "Kvalitetssäkrad",
                    date: "2021-02-15",
                    author: {
                      name: "Johan Asplund",
                      linkedin: "https://www.linkedin.com/in/bimjohan/"
                    }
                  }
            ],
            __filename: "nrb-tag-definition-nrb-" + swedishConcept.slug + '.json'
          };
          files.push(nrbTag);
    }

    if(item.en_term) {

        if(!item.en_source) {
            badConcepts.push({
                reason: "Missing source",
                concept: item
            });
            return;
        }
        
        if(!item.en_definition) {
            badConcepts.push({
                reason: "Missing definition",
                concept: item
            });
            return; 
        }

        var tmpStr = item.en_term + '-' + item.en_source + '-en';
        tmpStr = tmpStr.substr(0,60);
        var newSlug = slug(tmpStr);
        newSlug = newSlug.toLowerCase();

        englishConcept = {
            id: uuidv4(),
            _schema: "http://schemas.nationella-riktlinjer.se/nrb-concept.2.schema.json",
            slug: newSlug,
            language: "en",
            term: item.en_term.replace("\r\n", " ").trim(),
            source: item.en_source.replace("\r\n", " ").trim(),
            definition: item.en_definition.trim(),
            __filename: newSlug + '.json'
        };

        if(item.en_source_long) {
            englishConcept.source_long = item.en_source_long.replace("\r\n", " ").trim();
        }

        if(item.en_comment) {
            swedishConcept.comment = item.en_comment.replace("\r\n", " ").trim();
        }

        let qualityTag = {
            id: uuidv4(),
            _schema: "http://schemas.nationella-riktlinjer.se/nrb-tag.1.schema.json",
            tag_definition: "nrb-tag-definition-quality",
            concept: englishConcept.slug,
            language: "sv",
            review_steps: [
              {
                name: "Granskad",
                date: "2021-02-15",
                author: {
                  name: "Johan Asplund",
                  linkedin: "https://www.linkedin.com/in/bimjohan/"
                }
              }
            ],
            __filename: "nrb-tag-definition-quality-" + englishConcept.slug + '.json'
          };
          files.push(qualityTag);

          let nrbTag = {
            id: uuidv4(),
            _schema: "http://schemas.nationella-riktlinjer.se/nrb-tag.1.schema.json",
            tag_definition: "nrb-tag-definition-nrb",
            concept: englishConcept.slug,
            language: "sv",
            review_steps: [
                {
                    name: "Kandidat",
                    date: "2021-02-15",
                    author: {
                      name: "Johan Asplund",
                      linkedin: "https://www.linkedin.com/in/bimjohan/"
                    }
                  }, {
                    name: "Kvalitetssäkrad",
                    date: "2021-02-15",
                    author: {
                      name: "Johan Asplund",
                      linkedin: "https://www.linkedin.com/in/bimjohan/"
                    }
                  }
            ],
            __filename: "nrb-tag-definition-nrb-" + englishConcept.slug + '.json'
          };
          files.push(nrbTag);
    }

    // create links between them
    if(swedishConcept && englishConcept) {
        swedishConcept.synonyms = [englishConcept.slug];
        englishConcept.synonyms = [swedishConcept.slug];
    }

    if(swedishConcept) {
        files.push(swedishConcept);
    }

    if(englishConcept) {
        files.push(englishConcept);
    }

});

console.log(`Skipped ${skipped.length} rows`);
console.log(`Created ${files.length} new concepts`);

fs.writeFileSync('report/bad-concepts.json', JSON.stringify(badConcepts, null, 2));

_.forEach(files, (file, i) => {

    let filePath = 'output/' + file.__filename;
    delete file.__filename;
    
    fs.writeFileSync(filePath, JSON.stringify(file, null, 2));

});
